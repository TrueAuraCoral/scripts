echo "What folder to encrypt?"
read FOLDERNAME
cd
tar -cf $FOLDERNAME.tar.gz $FOLDERNAME
gpg -c --no-symkey-cache --cipher-algo AES256 ~/$FOLDERNAME.tar.gz
