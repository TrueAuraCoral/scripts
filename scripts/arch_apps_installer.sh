#!/bin/bash

echo "---------------------------"
echo "Welcome to my" 
echo "setup script"
echo "---------------------------"

APPS="htop brave-bin mpv newsboat gvim pcmanfm youtube-viewer sxiv ffmpeg groff ncmpcpp mpd zathura sc-im maim element-desktop"

#install paru
cd ~/opt
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si

#App_stuff
paru -Syu 
paru -S $APPS

#install sent
https://github.com/BigHeadGeorge/sent-pdf
cd sent
make
make install
